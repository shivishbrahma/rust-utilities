use std::fs::File;
use std::fs::OpenOptions;
use std::io::Write;
use std::io::{self, Read};

fn load_file() -> io::Result<(String, Vec<Vec<String>>)> {
    let template_pattern = {
        let mut file = File::open("input/template_create/template_pattern.rex")?;
        let mut content = String::new();
        file.read_to_string(&mut content)?;
        content
    };

    let template_words = {
        let mut file = File::open("input/template_create/template_words.rex")?;
        let mut content = String::new();
        file.read_to_string(&mut content)?;
        content
            .split("\n\n")
            .map(|seq| seq.trim().lines().map(|s| s.to_string()).collect())
            .collect()
    };

    Ok((template_pattern, template_words))
}

fn next_sequence(words: &Vec<Vec<String>>, index: usize) -> Vec<String> {
    words.iter().map(|w| w[index % w.len()].clone()).collect()
}

fn pattern_replace(pattern: &str, seq: &Vec<String>) -> String {
    let mut res = pattern.to_string();
    for (i, s) in seq.iter().enumerate() {
        res = res.replace(&format!("${}$", i), s);
    }
    res
}

pub fn run() -> io::Result<()> {
    let (template_pattern, template_words) = load_file()?;
    let max_len = template_words.iter().map(|w| w.len()).max().unwrap_or(0);

    let mut res = String::new();
    for i in 0..max_len {
        let seq = next_sequence(&template_words, i);
        res.push_str(&pattern_replace(&template_pattern, &seq));
        res.push_str("\n\n");
    }

    let mut file = OpenOptions::new()
        .append(true)
        .create(true)
        .open("output/template_create/template_output.rex")
        .expect("Unable to open file");
    file.write_all(res.as_bytes())
        .expect("Unable to write data");
    println!("Written to file successfully");
    Ok(())
}
